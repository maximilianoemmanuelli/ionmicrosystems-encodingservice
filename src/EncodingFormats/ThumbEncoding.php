<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 30/08/2017
 * Time: 04:08 PM
 */

namespace IonMicrosystems\EncodingService\EncodingFormats;


use IonMicrosystems\EncodingService\EncodingFormat;

class ThumbEncoding extends EncodingFormat
{
    protected $output = "thumbnail";

    protected $property  = [
        "ust_vtt"=>"yes",
        "time"=>"5!",
        "destination"=>""
    ];

    /**
     * ThumbEncoding constructor.
     */
    public function __construct()
    {
        return $this;
    }



}