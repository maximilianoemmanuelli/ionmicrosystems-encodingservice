<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 30/08/2017
 * Time: 01:45 PM
 */

namespace IonMicrosystems\EncodingService\EncodingFormats;


use IonMicrosystems\EncodingService\EncodingFormat;
use IonMicrosystems\EncodingService\iEncodingFormat;

class HlsEncoding extends EncodingFormat
{
    protected $output = "iphone_stream";
    private $defaultSizes = "0x240,0x288,0x360,0x432,0x480,0x576,0x720";


    protected $property = [
        'size'=>'',
        'bitrate'=>'',
        "audio_bitrate"=>'',
        "audio_sample_rate"=>'',
        "audio_channels_number"=>'',
        "keep_aspect_ratio"=>'',
        "video_codec"=>'',
        "profile"=>'',
        "audio_codec"=>'',
        "two_pass"=>'',
        "turbo"=>'',
        "twin_turbo"=>'',
        "cbr"=>'',
        "hard_cbr"=>'',
        "deinterlacing"=>'',
        "video_sync"=>'',
        "keyframe"=>'',
        "audio_volume"=>'',
        "rotate"=>'',
        "metadata_copy"=>'',
        "strip_chapters"=>'',
        "pix_format"=>'',
        "veryfast"=>'',
        "hint"=>'',
        "drm"=>'',
        "set_rotate"=>'',
        "segmenter"=>'',
        "bitrates"=>'',
        "framerates"=>'',
        "keyframes"=>'',
        "segment_duration"=>'',
        "duration_precision"=>'',
        "add_audio_only"=>'',
        "still_image"=>'',
        "still_image_time"=>'',
        "still_image_size"=>'',
        "encryption"=>'',
        "copy_nielsen_metadata"=>'',
        "pack_files"=>'',
        "destination"=>'',
    ];


    /**
     * HlsEncoding constructor.
     */
    public function __construct()
    {
        //Default Properties
        $this->size = $this->defaultSizes;

        $this->bitrate = "1024k";
        $this->audioBitrate = "56k";
        $this->audioSampleRate = "44100";
        $this->audioChannelsNumber = "2";
        $this->keepAspectRatio = "yes";
        $this->videoCodec = "libx264";
        $this->profile= "iphone_stream";
        $this->audioCodec= "dolby_heaac";
        $this->twoPass = "no";
        $this->turbo = "no";
        $this->twinTurbo = "no";
        $this->cbr = "no";
        $this->hardCbr = "no";
        $this->deinterlacing = "auto";
        $this->videoSync= "old";
        $this->keyframe = "300";
        $this->audioVolume = "100";
        $this->rotate = "def";
        $this->metadataCopy = "no";
        $this->stripChapters= "no";
        $this->pixFormat = "yuv420p";
        $this->veryfast = "auto";
        $this->hint = "no";
        $this->drm = "no";
        $this->setRotate = "0";
        $this->segmenter = "auto";
        $this->bitrates = "200k,420k,1000k,1400k,2000k,2600k,3400k";
        $this->framerates= "15,24,30,30,30,30,30";
        $this->keyframes= "45,72,90,90,90,90,90";
        $this->packFiles= "yes";
        $this->segmentDuration = "10";
        $this->durationPrecision = "0";
        $this->addAudioOnly = "yes";
        $this->stillImage = "no";
        $this->stillImageTime = "5";
        $this->stillImageSize= "480x320";
        $this->encryption = "no";
        $this->copyNielsenMetadata = "no";
    }







}