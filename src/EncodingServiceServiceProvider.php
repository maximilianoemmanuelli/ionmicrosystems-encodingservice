<?php

namespace IonMicrosystems\EncodingService;

use Illuminate\Support\ServiceProvider;

class EncodingserviceServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        if (function_exists('config_path')) {
            $this->publishes([__DIR__.'/config/config.php' => config_path('encoding_service.php'),], 'config');
        }

    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/config/config.php', 'shopping_cart');

        $this->app->singleton('encoding', function($app)
        {
            $storage = $app['session'];
            $events = $app['events'];
            $instanceName = 'encoding';
            return new EncodingService($storage, $events, $instanceName, config('encoding_service'));
        });


    }
}
