<?php
namespace IonMicrosystems\EncodingService;


class EncodingJob
{

    const JOB_ADD_MEDIA = "addMedia";

    private $action;
    private $source;
    private $formats;


    public function setAction($action){
        $this->action= $action;
        return $this;
    }

    public function setSource($source){
        $this->source = $source;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }


    public function addFormat(iEncodingFormat $format){
        $this->formats[] = $format;
        return $this;
    }

    public function getFormats()
    {
        $formats = [];
        foreach ($this->formats as $format) {
            $formats[] = $format->getEncodingData();
        }
        return $formats;
    }

}