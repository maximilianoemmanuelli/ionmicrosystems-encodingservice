<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 30/08/2017
 * Time: 11:36 AM
 */

namespace IonMicrosystems\EncodingService;


class EncodingService
{
    /**
     * the item storage
     *
     * @var
     */
    protected $session;
    /**
     * the event dispatcher
     *
     * @var
     */
    protected $events;
    /**
     * the cart session key
     *
     * @var
     */
    protected $instanceName;

    protected $config;
    /**
     * our object constructor
     *
     * @param $session
     * @param $events
     * @param $instanceName
     * @param $config
     */
    public function __construct($session, $instanceName, $events, $config)
    {
        $this->events = $events;
        $this->session = $session;
        $this->instanceName = $instanceName;
        //$this->fireEvent('created');
        $this->config = $config;
    }

    /**
     * get instance name of the cart
     *
     * @return string
     */
    public function getInstanceName()
    {
        return $this->instanceName;
    }

    public function getDemo(){
        return $this->config['ENCODING_CLIENT_ID'];
    }


    public function addJobQueue(EncodingJob $job)
    {
        $formats = $job->getFormats();

        $json = [
            "userid" => $this->config['ENCODING_CLIENT_ID'],
            "userkey" => $this->config['ENCODING_CLIENT_SECRET'],
            "action"=>$job->getAction(),
            "source"  => $job->getSource(),
            "format"=>$formats,
        ];

        return $json;

    }



}