<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 30/08/2017
 * Time: 01:43 PM
 */

namespace IonMicrosystems\EncodingService;


interface iEncodingFormat
{
    public function getEncodingData();

}