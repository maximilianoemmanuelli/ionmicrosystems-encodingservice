<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 30/08/2017
 * Time: 04:06 PM
 */

namespace IonMicrosystems\EncodingService;


abstract class EncodingFormat implements iEncodingFormat
{
    protected $output;
    protected $property = [];

    public function __call($name, $arguments)
    {
        $sname = snake_case($name);
        $f = explode("_",$sname);
        if ($f[0]=="set"){
            unset($f[0]);
            $variable = implode("_",$f);
            if (isset($this->property[$variable])){
                $this->property[$variable] = $arguments[0];
            }else{
                throw new EncodingMethodNotFoundException();
            }
            return $this;
        }

    }


    public function __set($name, $value) {
        $snake = snake_case($name);
        if (isset($this->property[$snake])){
            $this->property[$snake] = $value;
        }else{
            throw new EncodingPropertyNotFoundException();
        }

    }

    public function getOutput(){
        return $this->output;
    }

    public function getEncodingData()
    {
        $def = [
            "output" => $this->output,
        ];

        return array_merge($def,$this->property);
    }

}