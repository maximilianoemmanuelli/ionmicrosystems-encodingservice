<?php
return [
    'ENCODING_CLIENT_ID' => env('ENCODING_CLIENT_ID', ''),
    'ENCODING_CLIENT_SECRET' => env('ENCODING_CLIENT_SECRET', ''),
];