<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 30/08/2017
 * Time: 11:39 AM
 */

namespace IonMicrosystems\EncodingService\Facade;


use Illuminate\Support\Facades\Facade;

class EncodingServiceFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'encoding';
    }
}